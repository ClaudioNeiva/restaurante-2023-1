package br.ucsal.testequalidade.restaurante.domain;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import br.ucsal.testequalidade.restaurante.enums.SituacaoComandaEnum;

public class Comanda {

	private static Integer seq = 0;

	private Integer codigo;

	private Mesa mesa;

	private Map<Item, Integer> itens = new HashMap<>();

	private SituacaoComandaEnum situacao = SituacaoComandaEnum.ABERTA;

	public Comanda() {
		super();
		codigo = ++seq;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public Mesa getMesa() {
		return mesa;
	}

	public void setMesa(Mesa mesa) {
		this.mesa = mesa;
	}

	public Map<Item, Integer> getItens() {
		return itens;
	}

	public void setItens(Map<Item, Integer> itens) {
		this.itens = itens;
	}

	public SituacaoComandaEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoComandaEnum situacao) {
		this.situacao = situacao;
	}

	@Override
	public int hashCode() {
		return Objects.hash(codigo, itens, mesa, situacao);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comanda other = (Comanda) obj;
		return Objects.equals(codigo, other.codigo) && Objects.equals(itens, other.itens)
				&& Objects.equals(mesa, other.mesa) && situacao == other.situacao;
	}

	public Comanda(Integer codigo, Mesa mesa, Map<Item, Integer> itens, SituacaoComandaEnum situacao) {
		super();
		this.codigo = codigo;
		this.mesa = mesa;
		this.itens = itens;
		this.situacao = situacao;
	}

	@Override
	public String toString() {
		return "Comanda [codigo=" + codigo + ", mesa=" + mesa + ", itens=" + itens + ", situacao=" + situacao + "]";
	}

}
