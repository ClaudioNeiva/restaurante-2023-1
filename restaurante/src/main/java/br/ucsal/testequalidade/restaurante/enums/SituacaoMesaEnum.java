package br.ucsal.testequalidade.restaurante.enums;

public enum SituacaoMesaEnum {
	LIVRE, OCUPADA;
}
